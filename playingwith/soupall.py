from bs4 import BeautifulSoup
from fractions import Fraction
import urllib2
import re

ingredient_qty_re = re.compile("^((\d+)\s(\d+/\d+))|(\d+/\d+)|(\d+(\.\d+)?)")
ingredient_unit_re = re.compile("gallon|pint|cup|tablespoon|teaspoon|tbsp|tsp|ounce")
def parse_QtyString(qtyString):
    match = ingredient_qty_re.match(qtyString)
    for group in match.groups():
        if group != None:
            slash = group.find('/')       
            space = group.find(' ')
            if slash != -1 and space != -1:
                return float(qtyString[:space]) + float(Fraction(qtyString[space + 1:]))
            elif slash != -1:
                return float(Fraction(group))
            else:
                return float(group)

def parse_ingredient_string(ingredientString):
    qtyMatch = ingredient_qty_re.match(ingredientString)
    qtyString = "1"
    lastChar = 0
    if qtyMatch != None:
        qtyString = qtyMatch.group(0)
        lastChar = qtyMatch.span()[1]

    qty = parse_QtyString(qtyString)
    
    unitMatch = ingredient_unit_re.search(ingredientString)
    unit = ""
    if unitMatch != None:
        unit = unitMatch.group(0)
        lastChar = unitMatch.span()[1]
        if unit == "tbsp":
            unit = "tablespoon"
        elif unit == "tsp":
            unit = "teaspoon"

    ingredientName = ingredientString[lastChar+1:].strip()
    return {
        "name":ingredientName,
        "unit":unit,
        "quantity":qty
    }
 
def get_recipes_from_allrecipes():
    url = "http://allrecipes.com"
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page.read(), "lxml")
    recipes_init = soup.find_all(href=re.compile("^/recipe/[0-9]+/"))

    recipes_after = []
    for recipe in recipes_init:
        if recipe["href"] not in recipes_after:
            recipes_after.append(recipe["href"])
    return recipes_after

   
def recipe_from_link_all(link):
    print(link)
    page = urllib2.urlopen("http://allrecipes.com" + link)
    soup = BeautifulSoup(page.read(), "lxml")
    
    recipe = ((soup.find_all(property="og:title")[0]['content'])).split("Recipe")[0]


    prep = int(soup.find_all(itemprop="prepTime")[0].span.getText())
    cook = int(soup.find_all(itemprop="cookTime")[0].span.getText())

    ingredients = []
    for item in soup.find_all(class_="recipe-ingred_txt added", itemprop="ingredients"):
        ingredients.append(parse_ingredient_string(item.getText()))
        
    directions = []
    directionsText = soup.find_all(class_="recipe-directions__list--item")
    for direction in directionsText:
        directions.append(direction)

    return {
        "title":recipe,
        "prepTime":prep,
        "cookTime":cook,
        "ingredients":ingredients,
        "directions":directions
    }


