from bs4 import BeautifulSoup
from fractions import Fraction
import urllib2
import re

ingredient_qty_re = re.compile("^((\d+)\s(\d+/\d+))|(\d+/\d+)|(\d+(\.\d+)?)")
ingredient_unit_re = re.compile("gallon|pint|cup|tablespoon|teaspoon|tbsp|tsp|ounce")

def parse_QtyString(qtyString):
    match = ingredient_qty_re.match(qtyString)
    for group in match.groups():
        if group != None:
            slash = group.find('/')       
            space = group.find(' ')
            if slash != -1 and space != -1:
                return float(qtyString[:space]) + float(Fraction(qtyString[space + 1:]))
            elif slash != -1:
                return float(Fraction(group))
            else:
                return float(group)


def parse_bon_qty_string(qtyString)
    re.sub(r'\xbd', '.5', qtyString)

def parse_ingredient_bon(item):
    ingredientName = None
    unit = None
    qty = None

    for child in item.find_all("span"):
        if child["class"] == "quantity":
            qty = parse_bon_qty_string(child.contents)
        elif child["class"] == "unit":
            unit = child.contents
        elif child["class"] == "name":
            ingredientName = child.contents
    
    print({
        "name":ingredientName,
        "unit":unit,
        "quantity":qty
    })
 
def get_recipes_from_bonappetit():
    url = "http://bonappetit.com/magazine/march-2016"
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page.read(), "lxml")
    recipes_init = soup.find_all(href=re.compile("^http://www.bonappetit.com/recipe/"))

    recipes_after = []
    for recipe in recipes_init:
        if recipe["href"] not in recipes_after:
            recipes_after.append(recipe["href"])
    return recipes_after


   
def recipe_from_link_bonappetit(link):
    page = urllib2.urlopen(link)
    soup = BeautifulSoup(page.read(), "lxml")
    
    recipe = soup.title.string

    ingredients = []
    for item in soup.find_all(class_="ingredient"):
        ingredients.append(parse_ingredient_bon(item))
        
    directions = []
    directionsText = soup.find_all(class_="recipe-directions__list--item")
    for direction in directionsText:
        directions.append(direction)

    return {
        "title":recipe,
        "ingredients":ingredients,
        "directions":directions
    }

recipes = get_recipes_from_bonappetit()
for recipe in recipes:
    recipe_from_link_bonappetit(recipe)
