from django import forms

class SignUp(forms.Form):
    username = forms.CharField(max_length = 32, required = True)
    fname = forms.CharField(max_length = 32, required = True)
    lname = forms.CharField(max_length = 32, required = True)
    email = forms.CharField(max_length = 128, required = True)

