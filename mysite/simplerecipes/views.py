from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Max
from bs4 import BeautifulSoup
import json
from .models import *
import hashlib
import os
from django.db.models import Q, F
import urllib2
import datetime
from fractions import Fraction
import re

volume_conversions = {
    "gallon": 768,
    "quart": 192,
    "pint": 96,
    "pound": 96,
    "cup": 48,
    "tablespoon": 3,
    "ounce": 6,
    "teaspoon": 1,
} # convert volume units to teaspoons.

def convert_units(amount, otherunit):
    if (otherunit == None):
        return amount
    else:
        return conversions[otherunit] * amount

#END

def index(request):
    user_list = User.objects.order_by('username')[:5]
    output = ', '.join([u.username for u in user_list])
    return HttpResponse(output)

def request_data(request):
    print "data requested"
    print request.body
    return json.loads(request.body)

def verify_session(request):
    print "checking session"
    print request.body
    json_data = request.body
    data = json.loads(json_data)
    session_id = data["userId"]
    print("SESSIONID:")
    print(session_id)
    try:
        username = LoggedInUsers.objects.get(sessionId=session_id).username.username
    except LoggedInUsers.DoesNotExist:
        print("NoSuchUser")
        username = ""
    print(username)
    return username

def verify_keys(data, keys):
    for key in keys:
        if key not in data:
            return False
    return True

def hashPassword(password, salt):
    return password

def make_response_from_dictionary(result):
    response = HttpResponse(json.dumps(result),content_type = 'application/jsn')
    response["Access-Control-Allow-Headers"] = "*"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    return response

def fail_with_invalid_keys():
    return make_response_from_dictionary({"result": "failure", "reason":"missing keys"})


def login(request):
    print "trying to login"
    data = request_data(request)
    try:
        user = User.objects.get(username=data["username"])
    except User.DoesNotExist:
        return make_response_from_dictionary({sessionid:"", reason:"nouser"})

    if user.passwordhash == hashPassword(data["password"], user.salt):
        session = LoggedInUsers(username = user, sessionId = os.urandom(40).encode('base-64')[:40])
        session.save()
        return make_response_from_dictionary({"sessionid":session.sessionId, "reason":""})
    else:
        return make_response_from_dictionary({"sessionid":"", "reason":"badpass"})

def logout(request):
    data = request_data(request)
    try:
        session = LoggedInUsers.objects.get(sessionId=data["userId"])
        session.delete()
        return make_response_from_dictionary({"reason":"loggedout"})
    except LoggedInUsers.DoesNotExist:
        return make_response_from_dictionary({"reason":"invalidsession"})

def fail_with_bad_session():
    return make_response_from_dictionary({"result": "failed", "reason": "bad session"})

def no_recipes_matched():
    return make_response_from_dictionary({"result": "failed", "reason": "no recipes matched query"})

ingredient_qty_re = re.compile("^((\d+)\s(\d+/\d+))|(\d+/\d+)|(\d+(\.\d+)?)")
ingredient_unit_re = re.compile("gallon|pint|cup|tablespoon|teaspoon|tbsp|tsp|ounce")
def parse_QtyString(qtyString):
    match = ingredient_qty_re.match(qtyString)
    for group in match.groups():
        if group != None:
            slash = group.find('/')
            space = group.find(' ')
            if slash != -1 and space != -1:
                return float(qtyString[:space]) + float(Fraction(qtyString[space + 1:]))
            elif slash != -1:
                return float(Fraction(group))
            else:
                return float(group)

def parse_ingredient_string(ingredientString):
    qtyMatch = ingredient_qty_re.match(ingredientString)
    qtyString = "1"
    lastChar = 0
    if qtyMatch != None:
        qtyString = qtyMatch.group(0)
        lastChar = qtyMatch.span()[1]

    qty = parse_QtyString(qtyString)

    unitMatch = ingredient_unit_re.search(ingredientString)
    unit = ""
    if unitMatch != None:
        unit = unitMatch.group(0)
        lastChar = unitMatch.span()[1]
        if unit == "tbsp":
            unit = "tablespoon"
        elif unit == "tsp":
            unit = "teaspoon"

    ingredientName = ingredientString[lastChar+1:].strip()
    return {
        "name":ingredientName,
        "unit":unit,
        "quantity":qty
    }

def get_popular_from_allrecipes():
    url = "http://allrecipes.com"
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page.read(), "lxml")
    recipes_init = soup.find_all(href=re.compile("^/recipe/[0-9]+/"))

    recipes_after = []
    for recipe in recipes_init:
        if recipe["href"] not in recipes_after:
            recipes_after.append(recipe["href"])
    return recipes_after


def recipe_from_link_all(link):
    print(link)
    url = "http://allrecipes.com" + link
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page.read(), "lxml")

    recipe = ((soup.find_all(property="og:title")[0]['content'])).split("Recipe")[0]

    prep = None
    try:
        prep = int(soup.find_all(itemprop="prepTime")[0].span.getText())
    except IndexError:
        prep = ""

    cook = None
    try:
        cook = int(soup.find_all(itemprop="cookTime")[0].span.getText())
    except IndexError:
        cook = ""

    ingredients = []
    for item in soup.find_all(class_="recipe-ingred_txt added", itemprop="ingredients"):
        ingredients.append(parse_ingredient_string(item.getText()))

    directions = []
    directionsText = soup.find_all(class_="recipe-directions__list--item")
    for direction in directionsText:
        directions.append(str(direction))

    imgurl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXUcjPBT1hUtbiXtE5LP1RMOG7oXc2pfcsck3Ugt3JIIsNmqtH0Q"
    try:
        imgurl = soup.find_all(class_="rec-photo")[0]["src"]
    except IndexError:
        pass

    return {
        "title":recipe,
        "prepTime":prep,
        "cookTime":cook,
        "ingredients":ingredients,
        "directions":directions,
        "imageurl":imgurl,
        "url":url
    }

def parse_bon_qty_string(qtyString):
    re.sub(r'\xbd', '.5', qtyString)
    return parse_QtyString(qtyString)

def parse_ingredient_bon(item):
    ingredientName = None
    unit = None
    qty = None

    for child in item.find_all("span"):
        if child["class"][0] == "quantity":
            try:
                qty = float(child.contents[0])
            except IndexError:
                qty = 0
            except ValueError:
                qty = 0
        elif child["class"][0] == "unit":
            try:
                unit = child.contents[0]
            except IndexError:
                unit = ""
        elif child["class"][0] == "name":
            try:
                ingredientName = child.contents[0]
            except IndexError:
                ingredientName = None

    if ingredientName != None:
        return {
            "name":ingredientName,
            "unit":unit,
            "quantity":qty
        }
    else:
        return None

def get_popular_from_bonappetit():
    url = "http://bonappetit.com/magazine/march-2016"
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page.read(), "lxml")
    recipes_init = soup.find_all(href=re.compile("^http://www.bonappetit.com/recipe/"))

    recipes_after = []
    for recipe in recipes_init:
        if recipe["href"] not in recipes_after:
            recipes_after.append(recipe["href"])
    return recipes_after



def recipe_from_link_bonappetit(link):
    page = urllib2.urlopen(link)
    soup = BeautifulSoup(page.read(), "lxml")

    recipe = soup.title.string

    ingredients = []
    for item in soup.find_all(class_="ingredient"):
        ingredient = parse_ingredient_bon(item)
        if ingredient:
            ingredients.append(ingredient)

    directions = []
    directionsText = soup.find_all(class_="recipe-directions__list--item")
    for direction in directionsText:
        directions.append(direction)

    imgurl = soup.find_all(class_="content-feature-image hidden-print")[0].picture.img["srcset"]

    return {
        "url":link,
        "title":recipe,
        "prepTime":"",
        "cookTime":"",
        "ingredients":ingredients,
        "directions":directions,
        "imageurl":imgurl
    }


def get_recipes_from_allrecipes():
    urls = get_popular_from_allrecipes()
    result = []
    for url in urls:
        try:
            ForeignRecipe.objects.get(url=url)
        except ForeignRecipe.DoesNotExist:
            result.append(recipe_from_link_all(url))
    return result

def get_recipes_from_bon_appetit():
    urls = get_popular_from_bonappetit()
    result = []
    for url in urls:
        result.append(recipe_from_link_bonappetit(url))
    return result

def get_popular_foreign_recipes():
    popular = []
    popular.extend(get_recipes_from_allrecipes())
#    popular.extend(get_recipes_from_bon_appetit())
    return popular
def convert(diff):
    if diff == "easy":
        return 1
    elif diff == "medium":
        return 2
    else:
        return 3
def add_recipe(recipe):
    try:
        oldrecipe = Recipe.objects.get(name=recipe["title"])
        return oldrecipe
    except Recipe.DoesNotExist:
        pass
    print("adding")
    print(recipe["title"])
    newrecipe = Recipe(name=recipe["title"].encode('ascii', 'ignore'), imageurl=recipe["imageurl"].encode('ascii', 'ignore'), preptime=recipe["prepTime"], cooktime=recipe["cookTime"], direction="\n".join(recipe["directions"]).encode('ascii', 'ignore'))
    newrecipe.save()
    id = newrecipe.recipeid
    for ingredient in recipe["ingredients"]:
        try:
            oldingredient = Ingredient.objects.get(name=ingredient["name"].encode('ascii', 'ignore'))
            inrec = Ingredientinrecipe(name=oldingredient, recipeid=newrecipe, unit = ingredient["unit"].encode('ascii', 'ignore'), amount = ingredient["quantity"])
            inrec.save()
        except Ingredient.DoesNotExist:
            newingredient = Ingredient(name=ingredient["name"].encode('ascii', 'ignore'))
            newingredient.save()
            inrec = Ingredientinrecipe(name=newingredient, recipeid=newrecipe, unit = ingredient["unit"].encode('ascii', 'ignore'), amount = ingredient["quantity"])
            inrec.save()
    return newrecipe

def add_or_touch(recipe):
    try:
        foreignrecipe = ForeignRecipe.objects.get(url=recipe["url"])
        foreignrecipe.viewcount += 1
        foreignrecipe.save()
    except ForeignRecipe.DoesNotExist:
        id = add_recipe(recipe)
        foreignrecipe = ForeignRecipe(url=recipe["url"], viewcount=1, recipeid=id)
        foreignrecipe.save()

def add_or_touch_foreign_recipes(popular):
    for recipe in popular:
        add_or_touch(recipe)


def get_banned_ingredients(user):
    banned_ingredients = user.allergicto.all()
    user_restrictions = user.hasrestriction.all()
    for restriction in user_restrictions:
        banned_ingredients = banned_ingredients | restriction.ingredients.all()
    return banned_ingredients.values_list('name')

def get_permitted_recipes_for_user(user):
    banned_ingredients = get_banned_ingredients(user)
    return Recipe.objects.exclude(recipeid__in=Ingredientinrecipe.objects.filter(name__in=banned_ingredients))

def get_popular_from_us(user):
    permitted = get_permitted_recipes_for_user(user)
    permitted.exclude(name__in=user.favorite.all().values_list('name'))
    return permitted

def make_favorite_set(user):
    return set(user.favorite.all())

def make_favorite_counts(ruser):
    counts = {}
    rfavorites = make_favorite_set(ruser)
    for user in User.objects.all():
        if user.username != ruser.username:
            favorites = make_favorite_set(user)
            counts[user.username] = len(favorites.intersection(rfavorites))
    return counts

def calculate_favorite_score(ruser, recipes):
    counts = make_favorite_counts(ruser)
    scores = {}
    for recipe in recipes:
        scores[recipe.recipeid] = 0
        for user in User.objects.all():
            if (user.username != ruser.username):
                scores[recipe.recipeid] += counts[user.username]
    return scores

def make_favorite_ingredient_set(user):
    result = set()
    for recipe in user.favorite.all():
        result |= set(recipe.ingredientin.all())
    return result

def calculate_ingredient_score(ruser, popular):
    scores = {}
    favorite_set = make_favorite_ingredient_set(ruser)
    for recipe in popular:
        scores[recipe.recipeid] = len(set(recipe.ingredientin.all()).intersection(favorite_set))
    return scores

def calculate_difficulty_score(ruser, popular):
    maxdifficulty = ruser.favorite.all().aggregate(difficulty_max=Max('difficulty'))['difficulty_max']
    if maxdifficulty == None:
        maxdifficulty = 2
    scores = {}
    for recipe in popular:
        difficulty = recipe.difficulty
        if difficulty == None:
            difficulty = 2
        scores[recipe.recipeid] = maxdifficulty - difficulty
    return scores

def make_favorite_category_counts(user):
    counts = {}
    for recipe in user.favorite.all():
        if recipe.category != None:
            if recipe.category not in counts:
                counts[recipe.category] = 1
            else:
                counts[recipe.category] += 1
    return counts

def calculate_category_score(ruser, popular):
    counts = make_favorite_category_counts(ruser)
    scores = {}
    for recipe in popular:
        if recipe.category != None:
            if recipe.category not in counts:
                scores[recipe.recipeid] = 0
            else:
                scores[recipe.recipeid] = counts[recipe.category]
        else:
            scores[recipe.recipeid] = 0
    return scores

def rank_for_user(user, popular):
    favorite_weight = 5
    ingredient_weight = 2
    difficulty_weight = 3
    category_weight = .1

    favorite_scores = calculate_favorite_score(user, popular)
    ingredient_scores = calculate_ingredient_score(user, popular)
    difficulty_scores = calculate_difficulty_score(user, popular)
    category_scores = calculate_category_score(user, popular)

    result = []
    for recipe in popular:
        id = recipe.recipeid
        score = favorite_weight * favorite_scores[id] + ingredient_weight * ingredient_scores[id] + difficulty_weight * difficulty_scores[id] + category_weight * category_scores[id]
        result.append({"score":score, "recipe":recipe})

    return result

def makeDict(obj):
    recipe = {}
    recipe["id"] = obj.recipeid
    recipe["name"] = obj.name
    recipe["prep"] = obj.preptime
    recipe["cook"] = obj.cooktime
    recipe["difficulty"] = obj.difficulty
    recipe["directions"] = obj.direction
    recipe["imageurl"] = obj.imageurl
    recipe["category"] = obj.category
    return recipe


def recommendation(request):
    maxRecommendations = 20
    username = verify_session(request)
    if username:
        try:
            user = User.objects.get(username=username)
            foreign_popular = get_popular_foreign_recipes()
            add_or_touch_foreign_recipes(foreign_popular)
            popular = get_popular_from_us(user)
            ranked = rank_for_user(user, popular)
            ranked.sort(key = lambda recipe: recipe["score"])
            lastRanked = ranked[:min(len(ranked), maxRecommendations)]
            return make_response_from_dictionary({"result":"success", "recommendations":[makeDict(r["recipe"]) for r in ranked]})
        except User.DoesNotExist:
            return make_response_from_dictionary({"result":"failed", "reason":"nosuchuser"})

    else:
        return fail_with_bad_session()


def updatecount(ings, factor, username):
    u = User.objects.get(username = username)
    print(ings)
    print(factor)
    print(username)

    ingnames = [i["name"] for i in ings]
    ingobjs = []
    for name in ingnames:
        ingobj = Ingredient.objects.get(name = name)
        ingobjs.append(ingobj)
    Userhistory.objects.filter(username = u, ingredientname__in=ingobjs).update(count = F("count") + factor)
    #u.history.filter(ingredientname__in=ingobjs).update(count = F("count") + factor)
    existing = [obj.ingredientname for obj in Userhistory.objects.filter(username = u)]
    for ing in ingobjs:
        if ing not in existing:
            newobj = Userhistory.objects.create(username = u, ingredientname = ing, count = factor)
            newobj.save()

def search(request):
    username = verify_session(request)
    if username:
        inglist = []
        data = request_data(request)
        pantry = []
        cannotuse = data["cannotuse"]
        mustuse = data["mustuse"]
        #updatecount(mustuse,2,username)
        #updatecount(cannotuse,-1,username)
        if pantry:
            pantry = data["pantry"]
            updatecount(pantry,1,username)
        recipe_list = []
        for r in Recipe.objects.filter(name__contains=data["namelike"]):
            chose = True
            print (r)
            ings = r.ingredientin.all().values_list('name')
            #ings = [obj.name for obj in objs]
            for ing in ings:
                if ing in cannotuse:
                    chose = False
                    break
                if ing not in pantry:
                    chose = False
                    break

#            if not chose:
#                continue

#      """   for ing in mustuse:
#                if ing not in ings:
#                    chose = False
#                    break"""

#           if not chose:
#               continue

            obj = {}
            obj["recipeId"] = r.recipeid
            obj["name"] = r.name
            obj["prep"] = r.preptime
            obj["cook"] = r.cooktime
            recipe_list.append(obj)

        result = {}
        result["recipes"] = recipe_list
        return make_response_from_dictionary(result)
    else:
        return fail_with_bad_session()

def recipe(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        rid = int(data["recipeId"])
        try:
            obj = Recipe.objects.get(recipeid = rid)
            result["name"] = obj.name
            result["prep"] = obj.preptime
            result["cook"] = obj.cooktime
            result["difficulty"] = obj.difficulty
            #result["directions"] = "NOT ENTERED"
            result["directions"] = obj.direction
            result["imgurl"] = obj.imageurl
            result["category"] = obj.category
            result["ingredients_in"] = []
            rid = obj.recipeid
            print rid;
            for ing in Ingredientinrecipe.objects.filter(recipeid = rid):
                result["ingredients_in"].append(ing.name.name)

        except Recipe.DoesNotExist:
            result["result"] = "failed"
            result["reason"] = "recipe id does not exist"
    else:
        return fail_with_bad_session()

    return make_response_from_dictionary(result)

def add_to_recipe(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        rname = data["name"]
        result["reason"] = "updated"
        try:
            temp = Recipe.objects.get(name = rname)
            print("got recipe name:%s" % (rname))
        except Recipe.DoesNotExist:
            newrecipe = Recipe(name=rname,preptime=int(data["prep"]),cooktime=int(data["cook"]),difficulty=convert(data["difficulty"]),direction=data["directions"],imageurl=data["imgurl"]);
            newrecipe.save()
            temp = Recipe.objects.get(name = rname)
            result["reason"] = "created"

        rid = temp.recipeid
        temp.direction = str(data["directions"])
        #print (data["directions"])
        temp.preptime=int(data["prep"])
        temp.cooktime=int(data["cook"])
        temp.difficulty=convert(data["difficulty"])
        temp.imageurl = data["imgurl"]
        temp.save()
        ingredients = data["ingredients"]
        for n in ingredients:
            ingobj = Ingredient.objects.get(name = n["name"])
            newrecipe = Recipe.objects.get(recipeid = rid)
            ing = Ingredientinrecipe(name = ingobj,recipeid=newrecipe,amount=float(n["amount"]),unit = n["measurement"])
            ing.save()
            result["result"] = "success"
    else:
        return fail_with_bad_session()

    return make_response_from_dictionary(result)

def delete_from_recipe(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        rid = data["recipeId"]
        ingredients = data["ingredients"]
        Ingredientinrecipe.objects.filter(name__in=ingredients,recipeid=rid).remove()
        result["result"] = "success"
    else:
        return fail_with_bad_session()

    return make_response_from_dictionary(result)

def updaterecipe(request):
    username = verify_session(username)
    result = {}
    if username:
        rid = int(data["recipeId"])
        name = data["name"]
        prep = data["prep"]
        cook = data["cook"]
        difficulty = data["difficulty"]
        directions = data["directions"]
        imgurl = data["imgurl"]
        category = data["category"]
        recipe = Recipe.objects.get(recipeid = rid)
        if name != null:
            recipe.name = name
        if prep != null:
            recipe.preptime = prep
        if cook != null:
            recipe.cooktime = cook
        if difficulty != null:
            recipe.difficulty = difficulty
        if directions != null:
            recipe.directions = directions
        if imgurl != null:
            recipe.imgurl = imgurl
        if category != null:
            recipe.category = category
        result["result"] = "success"
    else:
            result["result"] = "failed"
            result["reason"] = "invalid session"

    return make_response_from_dictionary(result)

def add_ingredients(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        #ingredients = data.getlist("ingredients")
        ingredients = data["ingredients"]
        print "inside add_ing"
        print ingredients
        for n in ingredients:
            ing = Ingredient(name=n)
            ing.save()
        result["result"] = "success"
    else:
        return fail_with_bad_session()

    return make_response_from_dictionary(result)

def get_ingredients(request):
   # username = verify_session(request)
    result = {}
    if True:#username:
        result["ingredients"] = []
        for ing in Ingredient.objects.all():
            result["ingredients"].append(ing.name)
    else:
        return fail_with_bad_session()

    return make_response_from_dictionary(result)

def delete_ingredients(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        ingredients = data["ingredients"]
        Ingredient.objects.filter(name__in=ingredients).delete()
        result["result"] = "success"
    else:
        return fail_with_bad_session()
    return make_response_from_dictionary(result)

def ingredients_trigger(ingredients):
    alling = [obj.name for obj in Ingredient.objects.all()]
    for ing in ingredients:
        if ing not in alling:
            i = Ingredient.create(name = ing)
            i.save()

def smart_shop(request):
    username = verify_session(request)
    if username:
        ingcount = {}
        u = User.objects.get(username = username)
        shopsize = 20
        for recipe in u.favorite.all():
            for ing in recipe.ingredientin.all():
                key = ing.name
                print key
                if ingcount.has_key(key):
                    ingcount[key] += 2
                else:
                    ingcount[key] = 2

#        for item in u.hasshoppinglist.all():
#            key = item.ingredientname
#            if ingcount.has_key(key):
#                ingcount[key] += 3
#            else:
#                ingcount[key] = 3

        for ing in Userhistory.objects.filter(username = u):
            key = ing.ingredientname.name
            count = ing.count
            print key
            if ingcount.has_key(key):
                ingcount[key] += count
            else:
                ingcount[key] = count

        top_ings = [pair[0] for pair in sorted(ingcount.items(), reverse=True, key=lambda x:x[1])[:shopsize]]
        result = {}
        result["result"] = "success"
        result["list"] = top_ings

    else:
        result["result"] = "failure"
        result["reason"] = "invalid session"

    return make_response_from_dictionary(result)

def get_restrictions(request):
    return make_response_from_dictionary({
        "restrictions":Dietaryrestriction.objects.all().values_list('name')
    })

def add_user(request):
    print "adding user"
    data = request_data(request)
    if not verify_keys(data, ["username", "password", "first", "last", "email"]):
        return fail_with_invalid_keys()

    salt = os.urandom(8)
    new_user = User(
            username = data["username"],
            passwordhash = hashPassword(data["password"], salt),
            fname = data["first"],
            lname = data["last"],
            email = data["email"]
    )
    new_user.save()
    failed = []
    for restriction in data["restrictions"]:
        try:
            new_user.hasrestrictions.add(Dietaryrestriction.objects.get(name=restriction))
        except Dietaryrestriction.DoesNotExist:
            failed.append(restriction)

    for ingredient in data["allergic"]:
        try:
            new_user.allergicto.add(Ingredient.objects.get(name=ingredient))
        except Ingredient.DoesNotExist:
            print("Impossible in AddUser")
            print(Ingredient)


    return make_response_from_dictionary({"result":"success", "failed":failed})

def get_user(request):
    print "getting user"
    data = request_data(request)
    username = verify_session(request)
    if username:
        print(username)
        try:
            user = User.objects.get(username=username)
            result = {
                "result":"success",
                "username":username,
                "first":user.fname,
                "last":user.lname,
                "email":user.email,
                "restrictions":list(user.hasrestriction.all())
            }
            return make_response_from_dictionary(result)
        except User.DoesNotExist:
            print("Impossible in getuser")
            return fail_with_bad_session()
    else:
        return fail_with_bad_session()

def update_user(request):
    print "update user"
    data = request_data(request)
    username = verify_session(request)
    if username:
        try:
            user = User.objects.get(username=username)
            if data["first"]:
                user.fname = data["first"]

            if data["last"]:
                user.lname = data["last"]

            if data["email"]:
                user.email = data["email"]

            if data["allergic"]:
                allprev = [obj.name for obj in user.allergicto.all()]
                for name in data["allergic"]:
                    if name not in allprev:
                        try:
                            ingredient = Ingredient.objects.get(name=name)
                            user.allergicto.add(ingredient)
                        except Ingredient.DoesNotExist:
                            pass

            if data["restrictions"]:
                user.hasrestriction = data["restrictions"]

            return make_response_from_dictionary({"success":"success"})
        except User.DoesNotExist:
            print("Impossible in getuser")
            return fail_with_bad_session()
    else:
        return fail_with_bad_session()



def add_to_pantry(request):
    username = verify_session(request)
    if username:
        result = {}
        data = request_data(request)
        ingredients = data["ingredients"]
        updatecount(ingredients,1,username)
        #trigger_ingredients(ingredients)
        ingnames = [i["name"] for i in ingredients]
        allpantry = [obj.ingredientname.name for obj in Inpantry.objects.filter(username = username)]
        pos = 0
        for ing in ingnames:
            if ing not in allpantry:
                u = User.objects.get(username = username)
                i = Ingredient.objects.get(name = ing)
                p = Inpantry(username = u,ingredientname = i,amount =float( ingredients[pos]["amount"]))

                p.save()
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)

def delete_from_pantry(request):
    username = verify_session(request)
    if username:
        result = {}
        data = request_data(request)
        ingredients = data["ingredients"]
        ingnames = [i["name"] for i in ings]
        ingobjs = []
        for name in ingnames:
            ingobj = Ingredient.objects.get(name = name)
            ingobjs.append(ingobj)
        for i in ingobjs:
            i.delete()
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)


def getpantry(request):
    username = verify_session(request)
    if username:
        result = {}
        ingredients = [obj.ingredientname for obj in Inpantry.objects.filter(username = username)]
        result["ingredients"] = ingredients
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)

def add_to_shoppinglist(request):
    username = verify_session(request)
    if username:
        result = {}
        data = request_data(request)
        ingredients = data["ingredients"]
        #updatecount(ingredients,username,1)
        trigger_ingredients(ingredients)
        allshop = [obj.ingredientname for obj in ShoppingList.objects.filter(username = username)]

        for ing in ingredients:
            if ing not in allshop:
                p = ShoppingList.create(username = username,ingredientname = ing)
                p.save()
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)

def delete_from_shoppinglist(request):
    username = verify_session(request)
    if username:
        result = {}
        data = request_data(request)
        ingredients = data["ingredients"]
        ShoppingList.objects.filter(username = username).ingredientname.filter(name__in=ingredients).delete()
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)


def getshoppinglist(request):
    username = verify_session(request)
    if username:
        result = {}
        ingredients = ShoppingList.objects.filter(username = username).ingredientname.all().values_list('name')
        result["ingredients"] = ingredients
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)

def setRating(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        rid = data["recipeId"]
        rating = 0.0
        if data["rating"] != "":
            rating = float(data["rating"])
        u = User.objects.get(username = username)
        r = Recipe.objects.get(recipeid = rid)
        RecipeRatings.objects.update_or_create(user = u, recipe_id = r, defaults = {'user':u, 'recipe_id': r, 'rating': rating})
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)


def getAvgRating(request):
    username = verify_session(request)
    result = {}
    if username:
        avg = 0.0
        asum = 0.0
        count = 0
        data = request_data(request)
        rid = data["recipeId"]
        ratings = RecipeRatings.objects.filter(recipe_id = rid).values_list("rating")
        print ratings
        for rating in ratings:
            asum += rating
            count += 1
        if count != 0:
            avg = asum/count
        result = {}
        result["rating"] = avg
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)


def addFavRecipe(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        rid = data["recipeId"]
        u = User.objects.get(username = username)
        r = Recipe.objects.get(recipeid = rid)
        u.favorite.add(r)
        result["result"] = "success"
    else:
        result["result"] = "failure"
        result["reason"] = "invalid session id"
    return make_response_from_dictionary(result)

def getFavRecipes(request):
    username = verify_session(request)
    result = {}
    if username:
        data = request_data(request)
        u = User.objects.get(username = username)
        recipes = []
        for obj in u.favorite.all():
            recipe = {}
            recipe["id"] = obj.recipeid
            recipe["name"] = obj.name
            recipe["prep"] = obj.preptime
            recipe["cook"] = obj.cooktime
            recipe["difficulty"] = obj.difficulty
            recipe["directions"] = obj.direction
            recipe["imageurl"] = obj.imageurl
            recipe["category"] = obj.category
            recipes.append(recipe)
        result["recipes"] = recipes
        result["result"] = "success"
    else:
            result["result"] = "failed"
            result["reason"] = "invalid session"

    return make_response_from_dictionary(result)
"""def signup_form(request):
    if request.method == 'GET':
        form = SignUp()
    else:
        form = SignUp(request.POST)

        if form.is_valid():
            username = form_cleaned_data['username']
            fname = form_cleaned_data['fname']
            lname = form_cleaned_data['lname']
            email = form_cleaned_data['email']
            return HttpResponseRedirect(reverse('post_detail',kwargs={'post_id':post.id}))

    return render(request,'signup/signup_form.html',{'form':form,})"""
