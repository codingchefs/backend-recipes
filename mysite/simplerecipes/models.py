# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#models
from __future__ import unicode_literals

from django.db import models

class Ingredient(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'Ingredient'

class User(models.Model):
    username = models.CharField(primary_key=True, max_length=32)
    passwordhash = models.CharField(max_length=32)
    fname = models.CharField(max_length=32)
    lname = models.CharField(max_length=32)
    email = models.CharField(unique=True, max_length=128)
    salt = models.CharField(max_length=8)

    allergicto = models.ManyToManyField('Ingredient', related_name='allergicusers')
    hasrestriction = models.ManyToManyField('Dietaryrestriction')
    favorite = models.ManyToManyField('Recipe')

    hasshoppinglist = models.ManyToManyField('Ingredient', through='ShoppingList', related_name='ownername')
    history = models.ManyToManyField('Ingredient', through='Userhistory', related_name='historyuser')
    inpantry = models.ManyToManyField('Ingredient', through='Inpantry', related_name='inpantry')


    class Meta:
        managed = True
        db_table = 'User'



class Dietaryrestriction(models.Model):
    name = models.CharField(primary_key=True, max_length=32)
    description = models.TextField(blank=True, null=True)
    ingredients = models.ManyToManyField('Ingredient')

    class Meta:
        managed = True
        db_table = 'DietaryRestriction'

class Inpantry(models.Model):
    username = models.ForeignKey('User', on_delete=models.CASCADE)
    ingredientname = models.ForeignKey('Ingredient', on_delete=models.CASCADE)
    amount = models.FloatField()

    class Meta:
        managed = True
        db_table = 'InPantry'



class Ingredientinrecipe(models.Model):
    name = models.ForeignKey('Ingredient', on_delete=models.CASCADE)
    recipeid = models.ForeignKey('Recipe', on_delete=models.CASCADE)
    amount = models.FloatField()
    unit = models.CharField(max_length=32)

    class Meta:
        managed = True
        db_table = 'IngredientInRecipe'

class LoggedInUsers(models.Model):
    username = models.ForeignKey('User', on_delete=models.CASCADE)
    sessionId = models.CharField(primary_key = True, max_length=40)

    class Meta:
        managed = True
        db_table = 'LoggedInUsers'

class ForeignRecipe(models.Model):
    url = models.CharField(primary_key='True', max_length=2083)
    viewcount = models.IntegerField(default=0)
    recipeid = models.OneToOneField('Recipe', on_delete=models.CASCADE)
    lastviewed = models.DateField(auto_now=True)
        class Meta:
            managed = True
            db_table = 'ForeignRecipe'
class Recipe(models.Model):
    recipeid = models.AutoField(db_column='recipeID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(unique=True, max_length=64)
    imageurl = models.CharField(max_length=2083) # https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers
    category = models.CharField(max_length=32)
    preptime = models.FloatField(blank=True, null=True)
    cooktime = models.FloatField(blank=True, null=True)
    difficulty = models.IntegerField(blank=True, null=True)
    direction = models.TextField()
    ratings = models.ManyToManyField('User',through = 'RecipeRatings')
    ingredientin = models.ManyToManyField('Ingredient', through='Ingredientinrecipe')

    class Meta:
        managed = True
        db_table = 'Recipe'


class RecipeRatings(models.Model):
    user = models.ForeignKey('User', on_delete = models.CASCADE)
    recipe = models.ForeignKey('Recipe',on_delete = models.CASCADE)
    rating = models.IntegerField(blank=True, null = True)

    class Meta:
        managed = True
        db_table = 'RecipeRating'

class ShoppingList(models.Model):
    username = models.ForeignKey('User', on_delete=models.CASCADE)
    ingredientname = models.ForeignKey('Ingredient', on_delete=models.CASCADE)
    quantity = models.FloatField()

    class Meta:
        managed = True
        db_table = 'ShoppingList'


class Userhistory(models.Model):
    username = models.ForeignKey('User', on_delete=models.CASCADE)
    ingredientname = models.ForeignKey('Ingredient', on_delete=models.CASCADE)
    count = models.IntegerField(null=False, default=0)

    class Meta:
        managed = True
        db_table = 'UserHistory'
        unique_together = ('username','ingredientname')


