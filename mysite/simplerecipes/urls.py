from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^search$', views.search, name='search'),
    url(r'^recipe$',views.recipe,name='recipe'),
    url(r'^ator$',views.add_to_recipe,name='addtorecipe'),
    url(r'^dfromr$',views.delete_from_recipe,name='deletefromrecipe'),
    url(r'^addingredients$',views.add_ingredients,name='addingredients'),
    url(r'^getingredients$',views.get_ingredients,name='getingredients'),
    url(r'^deleteingredients$',views.delete_ingredients,name='deleteingredients'),
    url(r'^login$',views.login,name='login'),
    url(r'^logout$',views.logout,name='logout'),
    url(r'^updaterecipe$',views.updaterecipe,name='updaterecipe'),
    url(r'^getfav$',views.getFavRecipes,name ='getfavrecipe'),
    url(r'^getpantry$',views.getpantry,name = 'getpantry'),
    url(r'^atopantry$',views.add_to_pantry,name='addtopantry'),
    url(r'^dfrompantry$',views.delete_from_pantry,name='dfrompantry'),
    url(r'^getavgrating$',views.getAvgRating,name='getavgrating'),
    url(r'^addfav$',views.addFavRecipe,name ='addfavrecipe'),
    url(r'^setrating$',views.setRating,name='setrating'),
    url(r'^getcart$',views.getshoppinglist,name='getshoppinglist'),
    url(r'^atocart$',views.add_to_shoppinglist,name='addtoshoppinglist'),
    url(r'^dfromcart$',views.delete_from_shoppinglist,name='deletefromshoppinglist'),
    url(r'^getsmartcart$',views.smart_shop,name='getsmartshoppinglist'),
    url(r'^signup$',views.add_user,name='signup')
]
